const express = require('express');
require('./db/mongoose');

const Task = require('./models/task');
const userRouter = require('./routers/user');
const taskRouter = require('./routers/task');

const app = express();
const port = process.env.PORT || 3000;

// app.use((req, res, next) => {
//     if(req.method === 'GET') {
//         res.send('GET requests are disabled.')
//     } else {
//         next();
//     }
// });

// app.use((req, res, next) => {
//     res.status(503).send('Site is currently down. Check back soon.')
// });

app.use(express.json());
app.use(userRouter);
app.use(taskRouter);

// Port
app.listen(port, () => {
    console.log(`Server is up on port ${port}`);
});

// const jwt = require('jsonwebtoken');

// const myFunction = async () => {
//     const token = jwt.sign({ _id: '5d2461a70ea47a1d82176cc5' }, 'thisismytoken', { expiresIn: '0 sec' });
//     console.log(token);
// }

// myFunction();